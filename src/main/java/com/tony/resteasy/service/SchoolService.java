/**
 * 
 */
package com.tony.resteasy.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * @author wangtianzhi
 *
 */
@Path("school")
public class SchoolService {
	@GET
	@Path("/school")
	public String getBooks() {
		return "school";
	}
}
