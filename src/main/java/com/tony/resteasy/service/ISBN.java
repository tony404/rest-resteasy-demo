/**
 * 
 */
package com.tony.resteasy.service;

/**
 * @author tony
 *
 */
public class ISBN {
	String isbn;
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public ISBN(String isbn){
		this.isbn = isbn;
	}
}
