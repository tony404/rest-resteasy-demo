/**
 * 
 */
package com.tony.resteasy.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * @author tony
 *
 */

@Path("library")
public class LibraryServiceImpl implements LibraryService {

	@Override
	@GET
	@Path("/books")
	//@Consumes("text/*")
	public String getBooks() {
		return "books";
	}

	@Override
	@GET
	@Path("/book/{isbn}")
	@Produces(MediaType.APPLICATION_JSON)
	public Book getBook(@PathParam("isbn") ISBN id) {
		//Book book = new Book(id, "java book");
		
		Book book = new Book();
		book.setId(id);
		book.setName("java book");
		return book;
	}
	

	@Override
	@POST
	@Path("/book/{isbn}")
	public void addBook(@PathParam("isbn") String id, @QueryParam("name") String name) {
	}

	@Override
	@DELETE
	@Path("/book/{id}")
	public void removeBook(@PathParam("id") String id) {
	}

}
