/**
 * 
 */
package com.tony.resteasy.service;

/**
 * @author tony
 *
 */
public class Book {
	ISBN id;
	String name;
	
	/*public Book(ISBN id, String name){
		this.id = id;
		this.name = name;
	}*/
	
	public ISBN getId() {
		return id;
	}
	public void setId(ISBN id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
