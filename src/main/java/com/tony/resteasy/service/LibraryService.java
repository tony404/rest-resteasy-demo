/**
 * 
 */
package com.tony.resteasy.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

/**
 * @author tony
 *
 */

public interface LibraryService {
	public String getBooks();
	public Book getBook(ISBN id);
	public void addBook(String id, String name);
	public void removeBook(String id);
}
